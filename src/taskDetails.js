import React from "react";
import { View, Text } from "react-native";

const TaskDetails = ({ route }) => {
    const { task } = route.params;
    return(
    <View>
        <Text>Nome da Tarefa: {task.title}</Text>
        <Text>Data: {task.date}</Text>
        <Text>Hora: {task.time}</Text>
        <Text>Local: {task.address}</Text>
    </View>
    );
}

export default TaskDetails;